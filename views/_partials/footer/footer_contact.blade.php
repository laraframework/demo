<div itemscope itemtype="http://schema.org/LocalBusiness">
	<span itemprop="name">{{ $settngz->company_name }}</span><br><br>
	<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
		<span itemprop="streetAddress">{{ $settngz->company_street }} {{ $settngz->company_street_nr }}</span><br>
		<span itemprop="postalCode">{{ $settngz->company_pcode }}</span>
		<span itemprop="addressLocality">{{ $settngz->company_city }}</span><br>
		<span itemprop="addressCountry">{{ $settngz->company_country }}</span>
	</div><br>
	<div>Tel: <a href="tel:{{ $settngz->company_telephone_clean }}"><span itemprop="telephone">{{ $settngz->company_telephone }}</span></a></div>
	<div>E-mail: <a href="mailto:{{ $settngz->company_email }}"><span itemprop="email">{{ $settngz->company_email }}</span></a></div>
</div>