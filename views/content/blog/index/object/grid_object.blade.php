<?php $gridcolumn = ($loop->index % $data->params->gridcols) + 1; ?>

<div data-aos="fade-up" data-aos-delay="{{ $gridcolumn * 100 }}">

	<article>

		<figure>
			<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">
				@if($obj->hasFeatured())
					@include('_img.lazy', ['lzobj' => $obj->featured, 'lzw' => 800, 'lzh' => 800])
				@elseif($obj->hasVideos())
					<img data-src="https://img.youtube.com/vi/{{ $obj->video->youtubecode }}/0.jpg"
					     alt=""
					     width="480" height="360" class="lazyload" />
				@else
					<img data-src="https://via.placeholder.com/800x800/e8ecf0/d4d8dc"
					     alt=""
					     width="800" height="800" class="lazyload" />
				@endif
			</a>
		</figure>

		<div class="grid-object-{{ $entity->getEntityKey() }} grey-light-bg p-t-20 p-b-20 m-b-30 text-center">
			<h3>
				<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">
					{{ $obj->title }}
				</a>
			</h3>
		</div>

	</article>

</div>
