<article>
	<div class="row m-b-40">

		@if($entity->hasImages() || $entity->hasVideos())

			<div class="col-sm-3">
				<figure>
					<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">
						@if($obj->hasFeatured())
							@include('_img.lazy', ['lzobj' => $obj->featured, 'lzw' => 800, 'lzh' => 800])
						@elseif($obj->hasVideos())
							<img data-src="https://img.youtube.com/vi/{{ $obj->video->youtubecode }}/0.jpg"
							     alt=""
							     width="480" height="360" class="lazyload" />
						@else
							<img data-src="https://via.placeholder.com/800x800/e8ecf0/d4d8dc"
							     alt=""
							     width="800" height="800" class="lazyload" />
						@endif
					</a>
				</figure>
			</div>

			<div class="col-sm-9">
				<h2>
					<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}" class="brand1">
						{{ $obj->title }}
					</a>
				</h2>
				<p>{!! $obj['lead'] !!}</p>
				<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">Lees meer</a>
			</div>

		@else

			<div class="col-sm-12">
				<h2>
					<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}" class="brand1">
						{{ $obj->title }}
					</a>
				</h2>
				<p>{!! $obj['lead'] !!}</p>
				<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">Lees meer</a>
			</div>

		@endif

	</div>
</article>

