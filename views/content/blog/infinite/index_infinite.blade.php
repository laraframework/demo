<section class="{{ $data->grid->module }}">
	<div class="{{ $data->grid->container }}">
		<div class="row">
			<div class="{{ $data->grid->contentCols }} main-content">
				<div class="row">
					<div class="{{ $data->grid->gridColumns }}" id="post-data">

						@include('content.' . $entity->getEntityKey() . '.infinite.loop_infinite')

					</div>
				</div>

				<div class="ajax-load text-center" style="display: none;">
					<i class="fa fa-spinner fa-spin fa-2x"></i>
				</div>

			</div>
		</div>
	</div>
</section>

