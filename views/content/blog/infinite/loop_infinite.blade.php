@foreach($data->objects as $obj)
	<div class="row">
		<div class="col-sm-12">

			<article>
				<div class="row m-b-40">

					<div class="col-sm-3">
						<figure>
							<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">
								@include('_img.lazy', ['lzobj' => $obj->featured, 'lzw' => 800, 'lzh' => 800])
							</a>
						</figure>
					</div>

					<div class="col-sm-9">
						<h2>
							<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}" class="brand1">
								{{ $obj->title }}
							</a>
						</h2>
						<p>{!! $obj['lead'] !!}</p>
						<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">Lees meer</a>
					</div>

				</div>
			</article>
		</div>
	</div>
@endforeach
