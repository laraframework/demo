<meta charset="UTF-8">

<title>Lara {{ $laraversion->major }}</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicons -->
<link rel="shortcut icon" href="{{ Theme::url('images/favicon.ico') }}">
<link rel="apple-touch-icon" href="{{ Theme::url('images/apple-touch-icon.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ Theme::url('images/apple-touch-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ Theme::url('images/apple-touch-icon-114x114.png') }}">

<!-- Web Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,700' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>

@if(config('app.env') == 'production')
	{!! Theme::css('css/app.min.css') !!}
@else
	{!! Theme::css('css/app.css?ver='.date("YmdHis")) !!}
@endif

