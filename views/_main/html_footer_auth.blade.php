@if(config('app.env') == 'production')
	{!! Theme::js('js/all.min.js') !!}
@else
	{!! Theme::js('js/all.js?ver='.date("YmdHis")) !!}
@endif
