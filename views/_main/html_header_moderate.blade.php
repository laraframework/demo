<meta charset="UTF-8">

<title>Moderator</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">


<!-- Favicons -->
<link rel="shortcut icon" href="{{ Theme::url('images/favicon.ico') }}">
<link rel="apple-touch-icon" href="{{ Theme::url('images/apple-touch-icon.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ Theme::url('images/apple-touch-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ Theme::url('images/apple-touch-icon-114x114.png') }}">

<!-- Web Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,400&display=swap" rel="stylesheet">

<link rel="stylesheet" href="{{ Theme::url('js/vendor/bootstrap-toggle/css/bootstrap-toggle.css') }}">
<link rel="stylesheet" href="{{ Theme::url('js/vendor/select2/select2.css') }}">

@if(config('app.env') == 'production')
	{!! Theme::css('css/app.min.css') !!}
@else
	{!! Theme::css('css/app.css?ver='.date("YmdHis")) !!}
@endif

{!! Theme::js('js/vendor/pace/pace.min.js') !!}
