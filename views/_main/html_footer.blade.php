<!-- JAVASCRIPT FILES -->

@if($settngz->addthis_id && strlen($settngz->addthis_id) > 3)
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid={{ $settngz->addthis_id }}"
	        async="async"></script>
@endif

@if(config('app.env') == 'production')
	{!! Theme::js('js/all.min.js') !!}
@else
	{!! Theme::js('js/all.js?ver='.date("YmdHis")) !!}
@endif

{!! Theme::js('js/vendor/lazysizes/lazysizes.min.js') !!}
{!! Theme::js('js/vendor/lazysizes/plugins/ls.unveilhooks.min.js') !!}

<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	//page is optimized for fast onload event
	lazySizesConfig.loadMode = 1;
</script>

{!! Theme::js('js/vendor/cookie/js.cookie.js') !!}
