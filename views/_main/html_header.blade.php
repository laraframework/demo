<meta charset="UTF-8">

@include('_partials._seo.seo_title')

<meta name="viewport" content="width=device-width, initial-scale=1.0">

{!! Theme::js('js/vendor/hinclude/hinclude.js') !!}

@include('_partials._seo.seo_meta_tags')

@include('_partials._seo.og_meta_tags')

@include('_partials._google.ga4')
@include('_partials._google.gtm1')

<!-- Favicons -->
<link rel="shortcut icon" href="{{ Theme::url('images/favicon.ico') }}">
<link rel="apple-touch-icon" href="{{ Theme::url('images/apple-touch-icon.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ Theme::url('images/apple-touch-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ Theme::url('images/apple-touch-icon-114x114.png') }}">

<!-- Web Fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,700' rel='stylesheet'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:700' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="{{ asset('assets/admin/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">

@if(config('app.env') == 'production')
	{!! Theme::css('css/app.min.css') !!}
@else
	{!! Theme::css('css/app.css?ver='.date("YmdHis")) !!}
@endif

<script src="{{ Theme::url('js/vendor/cookieconsent/cookieconsent.min.js') }}"></script>
<script>
	window.addEventListener("load", function(){
		window.cookieconsent.initialise({
			"content": {
				"message": "Deze website maakt gebruik van cookies om u de beste gebruikerservaring te bieden.",
				"dismiss": "Ik snap het",
				"link": "Lees meer",
				"href": "{{ url('/privacy') }}"
			}
		})});
</script>

{!! Theme::js('js/vendor/pace/pace.min.js') !!}
