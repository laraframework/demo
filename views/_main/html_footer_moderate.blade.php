<!-- JAVASCRIPT FILES -->


@if(config('app.env') == 'production')
	{!! Theme::js('js/all.min.js') !!}
@else
	{!! Theme::js('js/all.js?ver='.date("YmdHis")) !!}
@endif

<script src="{{ Theme::url('js/vendor/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ Theme::url('js/vendor/select2/select2.full.js') }}"></script>

<script>
	$(document).ready(function () {

		// Initiate select2
		$(".select2").select2();

		// Fade out alerts
		$('div.alert').not('.alert-important').delay(3000).fadeOut(500);
	});

</script>



